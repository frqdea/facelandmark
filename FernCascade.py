# Kaskada regresorow
#
# todo:
# czytanie, pisanie, predykcja
# klasa pojedynczego regresora Fern

import numpy as np
import cv2
from Utilities import ProjectShape, SimilarityTransform, calculateCovariance


class FernCascade:
    ferns_ = []
    second_level_num_ = None

    def __init__(self):
        x = 1

    def train(self, images, current_shapes, ground_truth_shapes, bounding_box, mean_shape,
              second_level_num, candidate_pixel_num, fern_pixel_num, curr_level_num, first_level_num):
        candidate_pixel_locations = np.zeros((candidate_pixel_num, 2), float)
        nearest_landmark_index = np.zeros((candidate_pixel_num, 1), float)
        regression_targets = np.zeros((len(current_shapes), 28, 2), float)
        self.second_level_num_ = second_level_num

        for i in range(0, len(current_shapes)):
            a = ProjectShape(ground_truth_shapes[i], bounding_box[i])
            b = ProjectShape(current_shapes[i], bounding_box[i])
            print len(a)
            print len(b)
            print len(regression_targets[i])
            regression_targets[i] = ProjectShape(ground_truth_shapes[i], bounding_box[i]) - ProjectShape(current_shapes[i], bounding_box[i])
            rotation = []
            scale = None
            SimilarityTransform(mean_shape, ProjectShape(current_shapes[i], bounding_box[i]), rotation, scale)
            cv2.transpose(rotation, rotation)
            regression_targets[i] = regression_targets[i] * scale * rotation

        for i in range(0, candidate_pixel_num):
            x = np.random.uniform(-1.0, 1.0)
            y = np.random.uniform(-1.0, 1.0)
            if x*x + y*y > 1.0:
                i = i-1
                continue
            min_dist = 1e10
            min_index = 0
            for j in range(0, len(mean_shape)):
                temp = pow(mean_shape[j][0]-x, 2.0) + pow(mean_shape[j][1]-y, 2.0)
                if temp < min_dist:
                    min_dist = temp
                    min_index = j
            candidate_pixel_locations[i][0] = x - mean_shape[min_index][0]
            candidate_pixel_locations[i][1] = y - mean_shape[min_index][1]
            nearest_landmark_index[i] = min_index

        densities = np.zeros((len(images), candidate_pixel_num), float)
        for i in range(0, len(images)):
            rotation = []
            scale = None
            temp = ProjectShape(current_shapes[i], bounding_box[i])
            SimilarityTransform(temp, mean_shape, rotation, scale)
            for j in range(0, candidate_pixel_num):
                project_x = rotation[0][0] * candidate_pixel_locations[j][0] + rotation[0][1] * candidate_pixel_locations[j][1]
                project_y = rotation[1][0] * candidate_pixel_locations[j][0] + rotation[1][1] * candidate_pixel_locations[j][1]
                project_x = scale * project_x * bounding_box[i].width / 2.0
                project_y = scale * project_y * bounding_box[i].height / 2.0
                index = nearest_landmark_index[j]
                real_x = project_x + current_shapes[i][index][0]
                real_y = project_y + current_shapes[i][index][1]
                real_x = max(0.0, min(float(real_x), len(images[i][0])-1.0))
                real_y = max(0.0, min(float(real_y), len(images[i])-1.0))
                densities[j].append(int(images[i][real_y][real_x]))

        covariance = np.zeros((candidate_pixel_num, candidate_pixel_num))
        for i in range(0, candidate_pixel_num):
            for j in range(0, candidate_pixel_num):
                correlation_result = calculateCovariance(densities[i], densities[j])
                covariance[i][j] = correlation_result
                covariance[j][i] = correlation_result

        prediction = np.zeros((len(regression_targets), 1))
        for i in range(0, len(regression_targets)):
            prediction = np.zeros((len(mean_shape), 2))
        ferns_ = [] #to ma byc wektor typu Fern ktorego jeszcze nie ma
        for i in range(0, second_level_num):
            temp = ferns_[i].Train(densities, covariance, candidate_pixel_locations, nearest_landmark_index,
                                   regression_targets, fern_pixel_num)
            for j in range(0, len(temp)):
                prediction[j] = prediction[j] + temp[j]
                regression_targets[j] = regression_targets[j] - temp[j]

        for i in range(0, len(prediction)):
            rotation = []
            scale = None
            SimilarityTransform(ProjectShape(current_shapes[i], bounding_box[i]), mean_shape, rotation, scale)
            cv2.transpose(rotation, rotation)
            prediction[i] = scale * prediction[i] * rotation

        return prediction
