# main
#
# todo:
# GUI?
# test mode

from CamStream import CamStream
from kivy.app import App
from TrainModel import TrainModel


class FaceLandmark():
    cam_number = 1


training_mode = 1

if training_mode == 1:
    train_model = TrainModel()
    train_model.train_regression()
else:
    face_landmark = FaceLandmark()
    cam_stream = CamStream(face_landmark.cam_number)
    cam_stream.start_stream()