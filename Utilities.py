# Utilities
#
# todo:
# nie dzialaja flagi do kowariancji z cv2

import numpy as np
import copy
import cv2
import math
from numpy import linalg as LA



def GetMeanShape(shapes, bounding_box):
    result = np.zeros((len(shapes[0]), 2), float)
    for i in range(0, len(shapes)):
        result = result + ProjectShape(shapes[i], bounding_box[i])
    result = 1.0 / len(shapes) * result
    return result


def ProjectShape(shape, bounding_box):
    temp = np.zeros((len(shape), 2), float)
    for j in range(0, len(shape)):
        temp[j][0] = (shape[j][0] - bounding_box.centroid_x) / (bounding_box.width / 2.0)
        temp[j][1] = (shape[j][0] - bounding_box.centroid_y) / (bounding_box.height / 2.0)
    return temp


def ReProjectShape(shape, bounding_box):
    temp = np.zeros((len(shape), 2), float)
    for j in range(0, len(shape)):
        temp[j][0] = shape[j][0] * bounding_box.width / 2.0 + bounding_box.centroid_x
        temp[j][1] = shape[j][1] * bounding_box.height / 2.0 + bounding_box.centroid_y
    return temp


def SimilarityTransform(shape1, shape2, rotation, scale):
    rotation = np.zeros((2, 2))
    scale = 0

    center_x_1 = 0
    center_y_1 = 0
    center_x_2 = 0
    center_y_2 = 0
    for i in range(0, len(shape1)):
        center_x_1 += shape1[i][0]
        center_y_1 += shape1[i][1]
        center_x_2 += shape2[i][0]
        center_y_2 += shape2[i][1]

    center_x_1 = center_x_1/len(shape1)
    center_y_1 = center_y_1/len(shape1)
    center_x_2 = center_x_2/len(shape2)
    center_y_2 = center_y_2/len(shape2)

    temp1 = copy.copy(shape1)
    temp2 = copy.copy(shape2)
    for i in range(0, len(shape1)):
        temp1[i][0] -= center_x_1
        temp1[i][1] -= center_y_1
        temp2[i][0] -= center_x_2
        temp2[i][1] -= center_y_2
    covariance1 = np.array([])
    covariance2 = np.array([])
    mean1 = np.array([])
    mean2 = np.array([])
    covariance1 = cv2.calcCovarMatrix(temp1, mean1, cv2.COVAR_COLS, covariance1)
    covariance2 = cv2.calcCovarMatrix(temp2, mean2, cv2.COVAR_COLS, covariance2)
    print covariance1
    print covariance2
    s1 = math.sqrt(LA.norm(covariance1))
    s2 = math.sqrt(LA.norm(covariance2))

    # scale = s1/s2
    for i in range(0, 1):
        for j in range(0, len(temp1)):
            temp1[j][i] = 1.0 / s1 * temp1[j][i]
            temp2[j][i] = 1.0 / s2 * temp2[j][i]

    num = 0
    den = 0
    for i in range(0, len(shape1)):
        num = num + temp1[i][1] * temp2[i][0] - temp1[i][0] * temp2[i][1]
        den = den + temp1[i][0] * temp2[i][0] + temp1[i][1] * temp2[i][1]

    norm = math.sqrt(num*num + den*den)
    sin_theta = num/norm
    cos_theta = den/norm
    rotation[0][0] = cos_theta
    rotation[0][1] = -sin_theta
    rotation[1][0] = sin_theta
    rotation[1][1] = cos_theta


def calculateCovariance(v_1, v_2):
    v1 = copy.copy(v_1)
    v2 = copy.copy(v_2)

    mean_1 = cv2.mean(v1)[0]
    mean_2 = cv2.mean(v2)[0]
    for i in range(0, len(v1)):
        v1[i] = v1[i] - mean_1
        v2[i] = v2[i] - mean_2
    return cv2.mean(v1.mul(v2))[0]
