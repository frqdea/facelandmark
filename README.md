WIP

Facial landmarking program, tracks 28 facial landmarks (corners of lips, eyes, eyebrows etc.) from a video stream using explicit shape regression

"Face Alignment by Explicit Shape Regression" by Xudong Cao et al.