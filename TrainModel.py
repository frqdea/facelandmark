# DONE
#
# TrainModel to szkielet modelu treningowego, wczytuje dataset COFW i uruchamia trening ShapeRegressor
import cv2
import numpy as np
from ShapeRegressor import ShapeRegressor


class TrainModel:
    def __init__(self):
        self.img_num = 1345
        self.candidate_pixel_num = 400
        self.fern_pixel_num = 5
        self.first_level_num = 10
        self.second_level_num = 500
        self.landmark_num = 29
        self.initial_number = 20
        self.bbox = []
        self.images = []
        self.keypoints = []

    def train_regression(self):
        self.load_bounding_boxes_from_file("./data/COFW_Dataset/boundingbox.txt")
        self.load_images_from_dir("./data/COFW_Dataset/trainingImages/")
        self.load_keypoints_from_file("./data/COFW_Dataset/keypoints.txt")
        shape_regressor = ShapeRegressor()
        shape_regressor.train(self.images, self.keypoints, self.bbox, self.first_level_num, self.second_level_num,
                              self.candidate_pixel_num, self.fern_pixel_num, self.initial_number)
        shape_regressor.save("./model/model.txt")

    def load_images_from_dir(self, img_dir):
        for i in range(1, self.img_num):
            img_name = img_dir + str(i) + ".jpg"
            self.images.append(cv2.imread(img_name, 0))

    def load_bounding_boxes_from_file(self, filepath):
        with open(filepath) as file_in:
            bbox_coords = file_in.readlines()
        file_in.close()
        bbox_coords_stripped = [x.strip() for x in bbox_coords]
        temp = BoundingBox()
        for i in range(0, self.img_num):
            temp.start_x = int(bbox_coords_stripped[i].split('\t')[0])
            temp.start_y = int(bbox_coords_stripped[i].split('\t')[1])
            temp.width = int(bbox_coords_stripped[i].split('\t')[2])
            temp.height = int(bbox_coords_stripped[i].split('\t')[3])
            temp.centroid_x = temp.start_x + temp.width/2.0
            temp.centroid_y = temp.start_y + temp.height/2.0
            self.bbox.append(temp)

    def load_keypoints_from_file(self, filepath):
        with open(filepath) as file_in:
            keypoints = file_in.readlines()
        file_in.close()
        temp = np.zeros((self.landmark_num-1, 2), float)
        for i in range(0, self.img_num):
            for j in range(0, self.landmark_num-1):
                temp[j][0] = keypoints[i].split('\t')[j]
            for k in range(self.landmark_num, self.landmark_num*2-1):
                temp[k-self.landmark_num][1] = keypoints[i].split('\t')[k]
            self.keypoints.append(temp)


class BoundingBox:
    def __init__(self):
        self.start_x = 0
        self.start_y = 0
        self.width = 0
        self.height = 0
        self.centroid_x = 0
        self.centroid_y = 0
