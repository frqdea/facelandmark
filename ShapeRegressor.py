# ShapeRegressor zawiera metoda treningowa regresora
# uruchamia trening kaskady
#
# todo:
# pisanie modelu do pliku
# czytanie modelu z pliku
# metoda PREDICT do ustalania punktow (najpierw Fern)
import numpy as np
from FernCascade import FernCascade
from Utilities import ProjectShape, ReProjectShape, GetMeanShape


class ShapeRegressor:
    first_level_num_ = None
    landmark_num_ = None
    fern_cascades_ = []
    mean_shape_ = None
    training_shapes_ = None
    bounding_box_ = None

    def __init__(self):
        self.first_level_num_ = 0

    def train(
                self, images, ground_truth_shapes, bounding_box, first_level_num,
                second_level_num, candidate_pixel_num, fern_pixel_num, initial_num
    ):
        self.bounding_box_ = bounding_box
        self.training_shapes_ = ground_truth_shapes
        self.first_level_num_ = first_level_num
        self.landmark_num_ = len(ground_truth_shapes[0])
        augmented_images = []
        augmented_bounding_box = []
        augmented_ground_truth_shapes = []
        current_shapes = []

        for i in range(0, len(images)):
            for j in range(0, initial_num):
                index = 0
                while index == i:
                    index = np.random.uniform(0, len(images))
                augmented_images.append(images[i])
                augmented_ground_truth_shapes.append(ground_truth_shapes[i])
                augmented_bounding_box.append(bounding_box[i])
                temp = ground_truth_shapes[int(index)]
                temp = ProjectShape(temp, bounding_box[int(index)])
                temp = ReProjectShape(temp, bounding_box[i])
                current_shapes.append(temp)

        self.mean_shape_ = GetMeanShape(ground_truth_shapes, bounding_box)
        prediction = []
        for i in range(0, first_level_num):
            self.fern_cascades_.append(FernCascade())
            prediction = self.fern_cascades_[i].train(augmented_images, current_shapes, augmented_ground_truth_shapes,
                                                      augmented_bounding_box, self.mean_shape_, second_level_num,
                                                      candidate_pixel_num, fern_pixel_num, i+1, first_level_num)
            for j in range(0, len(prediction)):
                current_shapes[j] = prediction[j] + ProjectShape(current_shapes[j], augmented_bounding_box[j])
                current_shapes[j] = ReProjectShape(current_shapes[j], augmented_bounding_box[j])


    def save(self, filepath):
        x = 2
