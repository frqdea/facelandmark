# obsluga kamery
#
# todo:
# wyswietlanie landmarkow

import cv2


class CamStream:
    cam_num = -1

    def __init__(self, cam_num):
        self.cam_number = cam_num

    def start_stream(self):
        cap = cv2.VideoCapture(self.cam_number)

        while cap.isOpened():
            ret, frame = cap.read()

            # analyze frame
            # return and superimpose landmark coordinates onto frame

            if ret is True:
                cv2.imshow('Output image', frame)

            if cv2.waitKey(1) & 0xFF == ord('q'):
                break

        cap.release()
        cv2.destroyAllWindows()
